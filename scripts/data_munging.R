# clear the workspace
rm(list=ls())
# set the working directory
getwd()
pathDir<-"C:/Users/Ashoo/Documents/R playground/practical_machine_learning/my_demos"
setwd(pathDir)
rm(pathDir)
# 0. Postulate a research question
## 0.a. Determine the hypothesis (null and alternative)
# 1. Get the data 
## 1.a. From the web 
### read.csv("url_of_the_data"), read.table("url_of_the_data")
usedCars<-read.csv()
## 1.b. from local database 
## 1.c. from local machine
# 2. load the data
# 3. Data pre-processing: 
# 3.a. Data exploration: check the data type for all variables using str(), b. Take a five point summary using summary()
# 3.c. Check for missing data using sum(is.na(variableName))
# 3.d. Check the summary statistic for several numeric variables at the same time using summary(dataframe[c("predictor1","predictor2")])
# 3.e. Centering and Scaling the continuous variables 
